import 'dart:convert';
import 'package:http/http.dart' as http;

dynamic get(url) async {
  final response = await http.get(url);
  return json.decode(response.body);
}
