String validateEmail(String value) {
  if (value.isEmpty) {
    return 'The E-mail Address must be filled.';
  }
  if (value.length < 5) {
    return 'The E-mail Address must be a valid email address.';
  }
  return null;
}

String validatePassword(String value) {
  if (value.isEmpty) {
    return 'The Password must be filled.';
  }
  if (value.length < 8) {
    return 'The Password must be at least 8 characters.';
  }

  return null;
}
