import 'package:flutter/material.dart';

import 'package:flutter_navigation_widget/counter/index.dart';
import 'package:flutter_navigation_widget/screens/AnimatedIcon.page.dart';
import 'package:flutter_navigation_widget/screens/Dashboard.page.dart';
import 'package:flutter_navigation_widget/screens/Draggable.page.dart';
import 'package:flutter_navigation_widget/screens/FutureBuilder.page.dart';
import 'package:flutter_navigation_widget/screens/ListBeer.dart';
import 'package:flutter_navigation_widget/screens/MyHome.page.dart';
import 'package:flutter_navigation_widget/screens/Opacity.page.dart';
import 'package:flutter_navigation_widget/screens/Stack.page.dart';
import 'package:flutter_navigation_widget/screens/createAccount/createaccountForm.page.dart';
import 'package:flutter_navigation_widget/screens/pageView.page.dart';

class Routes {
  var routeList = <String, WidgetBuilder>{
    "/": (BuildContext context) => MyHome(),
    "/counter": (BuildContext context) => MyCounter(),
    "/dashboard": (BuildContext context) => DashBoard(),
    "/list":(BuildContext context) => ListBeer(),
    "/form": (BuildContext context) => FormPage(),
    "/stack": (BuildContext context) => MyStack(),
    '/future': (BuildContext context) => MyFutureBuilder(),
    '/opacity': (BuildContext context) => MyOpacity(),
    '/animation-icon': (BuildContext context) => MyAnimatedIcon(),
    '/page-view': (BuildContext context) => MyPageView(),
    '/drag': (BuildContext context) => MyDragable(),
  };
}
