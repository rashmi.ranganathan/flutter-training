import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/themes/theme.dart';
import './routes/routes.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Navigation and Widgets',
      theme: MyTheme.getThemes(),
      initialRoute: '/',
      routes: Routes().routeList,
    );
  }
}
