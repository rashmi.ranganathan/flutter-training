import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_navigation_widget/counter/counter_bloc.dart';

class ActionButton extends StatelessWidget {
  final CounterEvent event;

  ActionButton(this.event);

  @override
  Widget build(BuildContext context) {
    //ignore: close_sinks
    CounterBloc _counterBloc = BlocProvider.of<CounterBloc>(context);

    IconData getIcon(event) {
      if (event == CounterEvent.increment) {
        return Icons.add;
      } else {
        return Icons.remove;
      }
    }

    return FloatingActionButton(
      key: ValueKey('${event.toString()}_button'),
      heroTag: ValueKey(event.toString()),
      child: Icon(getIcon(event)),
      onPressed: () {
        _counterBloc.add(event);
      },
    );
  }
}
