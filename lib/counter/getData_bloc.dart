import 'package:bloc/bloc.dart';

enum DataEvent { update }

class DataBloc extends Bloc<DataEvent, String> {
  @override
  String get initialState => '';

  @override
  Stream<String> mapEventToState(DataEvent event) async* {
    switch (event) {
      case DataEvent.update:
        yield state + 'Hello World';
        break;
    }
  }

  // @override
  // void onTransition(Transition<DataEvent, String> transition) {
  //   print(transition);
  // }

  // @override
  // void onError(Object error, StackTrace stackTrace) {
  //   print('$error, $stackTrace');
  // }
}
