import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_navigation_widget/counter/action_button.dart';
import 'package:flutter_navigation_widget/counter/counter_bloc.dart';
// import 'package:flutter_navigation_widget/counter/getData_bloc.dart';

class MyCounterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Counter')),
      body: BlocBuilder<CounterBloc, int>(
        builder: (context, data) {
          return Center(child: Text("$data"));
        },
      ),
      floatingActionButton: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          ActionButton(CounterEvent.increment),
          ActionButton(CounterEvent.decrement),
        ],
      ),
    );
  }
}
