import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter_navigation_widget/counter/counter_bloc.dart';
import 'package:flutter_navigation_widget/counter/counter_screen.dart';
// import 'package:flutter_navigation_widget/counter/getData_bloc.dart';

class MyCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<CounterBloc>(
      create: (context) => CounterBloc(),
      child: MyCounterScreen(),
    );
  }
}
