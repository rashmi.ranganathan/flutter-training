import 'package:flutter/material.dart';

class MyBackGroundImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/new_york.jpg'),
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
