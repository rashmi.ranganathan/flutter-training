import 'package:flutter/material.dart';

Widget createNextButton(BuildContext context, Function next) {
  return Container(
    margin: EdgeInsets.only(top: 20),
    width: MediaQuery.of(context).size.width,
    child: RaisedButton(
        key: ValueKey('next_button'),
        color: Colors.blue,
        textColor: Colors.white,
        child: Text('Next'),
        onPressed: next),
  );
}
