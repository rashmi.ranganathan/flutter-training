import 'package:flutter/material.dart';

Widget getcompleteProgressWidget() {
  return Container(
    height: 45,
    decoration: new BoxDecoration(
      color: Colors.green,
      shape: BoxShape.circle,
    ),
    child: Icon(
      Icons.done,
      color: Colors.white,
    ),
  );
}

Widget getIncompleteProgressWidget(value) {
  return Container(
    height: 45,
    decoration: new BoxDecoration(
      color: Colors.white,
      shape: BoxShape.circle,
      border: Border.all(),
    ),
    child: Center(
      child: Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(color: Colors.black),
      ),
    ),
  );
}
