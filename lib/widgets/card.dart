import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/constants/constant.dart';

class MyCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: Card(
        elevation: 8.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
              child: Text(
                "New York",
                key: ValueKey('New_york_title'),
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                randomText,
                key: ValueKey('New_york_description'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
