import 'package:flutter/material.dart';

Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
  List<String> values = snapshot.data;
  return ListView.builder(
    itemCount: values.length,
    itemBuilder: (BuildContext context, int index) {
      return Column(
        children: <Widget>[
          ListTile(
            title: Text(values[index]),
          ),
          Divider(
            height: 2.0,
          ),
        ],
      );
    },
  );
}
