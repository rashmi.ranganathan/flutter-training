import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Tile extends StatelessWidget {
  final dynamic tile;

  const Tile({Key key, this.tile}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
            title: Text(tile.name),
            subtitle: Text(tile.tagline),
            leading: Container(
              margin: EdgeInsets.only(left: 6),
              child: Image.network(
                tile.imageUrl,
                height: 50,
                fit: BoxFit.fill,
              ),
            )),
        Divider()
      ],
    );
  }
}
