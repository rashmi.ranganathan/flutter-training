import 'package:flutter/material.dart';

class Loader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(padding: EdgeInsets.all(20)),
        Center(
          child: CircularProgressIndicator(),
        )
      ],
    );
  }
}
