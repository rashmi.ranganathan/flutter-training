import 'package:flutter/material.dart';

class MyGradiant extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: FractionalOffset.bottomRight,
          end: FractionalOffset.topLeft,
          colors: [Colors.blueGrey, Colors.white],
        ),
      ),
    );
  }
}
