import 'package:flutter/material.dart';

class MyAnimatedIcon extends StatefulWidget {
  @override
  _PlayPauseState createState() => _PlayPauseState();
}

class _PlayPauseState extends State<MyAnimatedIcon>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  bool isPlaying = false;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  void _handleOnPressed() {
    setState(() {
      isPlaying = !isPlaying;
      isPlaying
          ? _animationController.forward()
          : _animationController.reverse();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Animated Icon"),
      ),
      body: IconButton(
        key: ValueKey('Animated_button'),
        alignment: Alignment.center,
        iconSize: 200,
        icon: AnimatedIcon(
          icon: AnimatedIcons.play_pause,
          progress: _animationController,
        ),
        onPressed: () => _handleOnPressed(),
      ),
    );
  }
}
