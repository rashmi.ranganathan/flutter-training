import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/widgets/listView.dart';

Future<List<String>> _getData() async {
  var values = new List<String>();
  values.add("Horses");
  values.add("Goats");
  values.add("Chickens");
  await new Future.delayed(new Duration(seconds: 3));

  // throw ("Something went wrong!");

  return values;
}

class MyFutureBuilder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("FUTURE BUILDER"),
      ),
      body: FutureBuilder(
        future: _getData(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(child: Text('loading...'));
            case ConnectionState.done:
              if (snapshot.hasError)
                return Center(
                  child: Column(children: <Widget>[
                    Icon(Icons.error, color: Colors.red),
                    Text(snapshot.error)
                  ]),
                );
              else
                return createListView(context, snapshot);
              break;
            default:
              return Container();
          }
        },
      ),
    );
  }
}
