import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/constants/constant.dart';

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        key: Key('home_page'),
        title: Text('Home'),
      ),
      body: GridView.count(
        crossAxisCount: 2,
        children: List<Widget>.generate(routes.length, (index) {
          return RaisedButton(
            key: ValueKey('home_button_${routes[index]}'),
            child: Text('Go To ${routes[index]}'),
            onPressed: () {
              Navigator.pushNamed(context, '/${routes[index]}');
            },
          );
        }),
      ),
    );
  }
}
