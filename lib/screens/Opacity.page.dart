import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/widgets/card.dart';
import 'package:flutter_navigation_widget/widgets/gradiant.dart';
import 'package:flutter_navigation_widget/widgets/backgroundImage.dart';

class MyOpacity extends StatefulWidget {
  final String text;

  MyOpacity({this.text});

  @override
  _MyOpacityState createState() => _MyOpacityState();
}

class _MyOpacityState extends State<MyOpacity> {
  bool _visible = true;

  _onPressed() {
    // setState(() {
    //   _visible = !_visible;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("OPACITY"),
      ),
      body: Stack(
        children: <Widget>[
          MyBackGroundImage(), // background
          AnimatedOpacity(
            opacity: _visible ? 1.0 : 0.0, // toggle animation
            duration: Duration(seconds: 2), // duration
            child: MyGradiant(),
          ),
          Center(child: MyCard()), // card
          Align(
            alignment: Alignment.topCenter,
            child: Text(widget.text ?? ''),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        key: ValueKey('OPACITY_button'),
        // button
        onPressed: _onPressed(), // onclick
        child: Icon(Icons.flip),
      ),
    );
  }
}

// problem is when I save, it triggers the value of _visible
// Ideally it should trigger toogle on button click - not happening
