import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/screens/AnimatedIcon.page.dart';
import 'package:flutter_navigation_widget/screens/FutureBuilder.page.dart';
import 'package:flutter_navigation_widget/screens/Opacity.page.dart';
import 'package:flutter_navigation_widget/screens/Stack.page.dart';

class MyPageView extends StatelessWidget {
  final _controller = PageController(initialPage: 0);
  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _controller,
      children: <Widget>[
        PageView(
          scrollDirection: Axis.vertical,
          children: <Widget>[
            MyStack(),
            MyFutureBuilder(),
            MyAnimatedIcon(),
            MyOpacity(),
          ],
        ),
      ],
    );
  }
}
