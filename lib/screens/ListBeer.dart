import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/models/beer.dart';
import 'package:flutter_navigation_widget/services/beer.service.dart';
import 'package:flutter_navigation_widget/widgets/loader.dart';
import 'package:flutter_navigation_widget/widgets/tile.dart';

class ListBeer extends StatefulWidget {
  @override
  _ListBeerState createState() => _ListBeerState();
}

class _ListBeerState extends State<ListBeer> {
  List<Beer> _beers = <Beer>[];
  @override
  void initState() {
    super.initState();
    listenForBeers();
  }

  bool hasReceivedBeers() {
    return _beers.length > 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("The list of amazing beers"),
      ),
      body: hasReceivedBeers() ? myBeerList(_beers) : Loader(),
    );
  }

  void listenForBeers() async {
    final List<Beer> beerList = await getBeers();
    beerList.forEach((Beer beer) => setState(() => _beers.add(beer)));
  }
}

Widget myBeerList(List<Beer> beers) {
  return ListView.builder(
    padding: EdgeInsets.all(16),
    itemCount: beers.length,
    itemBuilder: (BuildContext context, int index) {
      return Tile(
        tile: beers[index],
      );
    },
  );
}
