import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/util/createAccountValidation.dart';
import 'package:flutter_navigation_widget/widgets/nextButton.dart';

class ScheduleEvent extends StatefulWidget {
  final Function incrementPage;

  ScheduleEvent({Key key, @required this.incrementPage}) : super(key: key);

  @override
  ScheduleEventState createState() => ScheduleEventState();
}

class ScheduleEventState extends State<ScheduleEvent> {
  final _scheduleEvent = GlobalKey<FormState>();
  bool _showPassword = true;
  String password;

  _setPassword(String value) {
    setState(() {
      password = value;
    });
  }

  _toggleShowPassword() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _scheduleEvent,
      autovalidate: true,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          TextFormField(
            onChanged: _setPassword,
            validator: (String value) => validatePassword(value),
            obscureText: _showPassword,
            decoration: InputDecoration(
              hintText: "Password",
              labelText: "Password",
              suffixIcon: GestureDetector(
                onTap: _toggleShowPassword,
                onPanStart: null,
                child: _showPassword
                    ? Icon(Icons.visibility)
                    : Icon(Icons.visibility_off),
              ),
            ),
          ),
          createNextButton(context, _next)
        ],
      ),
    );
  }

  void _next() {
    if (_scheduleEvent.currentState.validate()) {
      _scheduleEvent.currentState.save();
      widget.incrementPage();
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Not valid!')));
    }
  }
}
