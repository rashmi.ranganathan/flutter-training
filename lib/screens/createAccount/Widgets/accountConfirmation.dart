import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/constants/constant.dart';
import 'package:flutter_navigation_widget/widgets/progress.dart';

class AccountConfirmation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: accountCreationSteps.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return ListTile(
              leading: getcompleteProgressWidget(),
              title: Text(accountCreationSteps[index]),
            );
          }),
    );
  }
}
