import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/util/createAccountValidation.dart';
import 'package:flutter_navigation_widget/widgets/nextButton.dart';

class AccountPassword extends StatefulWidget {
  final Function incrementPage;

  AccountPassword({Key key, @required this.incrementPage}) : super(key: key);

  @override
  AccountPasswordState createState() => AccountPasswordState();
}

class AccountPasswordState extends State<AccountPassword> {
  final _accountPassword = GlobalKey<FormState>();
  bool _showPassword = true;
  String password;

  _setPassword(String value) {
    setState(() {
      password = value;
    });
  }

  _toggleShowPassword() {
    setState(() {
      _showPassword = !_showPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _accountPassword,
      autovalidate: true,
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          TextFormField(
            onChanged: _setPassword,
            validator: (String value) => validatePassword(value),
            obscureText: _showPassword,
            decoration: InputDecoration(
              hintText: "Password",
              labelText: "Password",
              suffixIcon: GestureDetector(
                onTap: _toggleShowPassword,
                onPanStart: null,
                child: _showPassword
                    ? Icon(Icons.visibility)
                    : Icon(Icons.visibility_off),
              ),
            ),
          ),
          createNextButton(context, _next)
        ],
      ),
    );
  }

  void _next() {
    if (_accountPassword.currentState.validate()) {
      _accountPassword.currentState.save();
      widget.incrementPage();
    }
  }
}
