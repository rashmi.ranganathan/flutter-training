import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/constants/constant.dart';
import 'package:flutter_navigation_widget/widgets/progress.dart';

class FormProgress extends StatelessWidget {
  final progress;

  FormProgress({Key key, @required this.progress}) : super(key: key);

  Widget getProgressbar(progress, index) {
    Widget progressCircle = index < progress
        ? getcompleteProgressWidget()
        : getIncompleteProgressWidget((index + 1).toString());

    return Expanded(child: progressCircle);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: List.generate(
        accountCreationSteps.length,
        (index) => getProgressbar(progress, index),
      ),
    );
  }
}
