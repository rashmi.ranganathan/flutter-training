import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/util/createAccountValidation.dart';
import 'package:flutter_navigation_widget/widgets/nextButton.dart';

class AccountEmail extends StatefulWidget {
  final Function incrementPage;

  AccountEmail({@required this.incrementPage});

  @override
  _AccountEmailState createState() => _AccountEmailState();
}

class _AccountEmailState extends State<AccountEmail> {
  final _accountEmailFormKey = GlobalKey<FormState>();
  bool showPassword = true;
  String email;

  _setEmail(String value) {
    setState(() {
      email = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        // Transform.rotate(
        //   angle: 3 / 12,
        //   child: Container(
        //     child: new OverflowBox(
        //       minWidth: 0.0,
        //       minHeight: 0.0,
        //       maxWidth: double.infinity,
        //     ),
        //     // margin: const EdgeInsets.all(20.0),
        //     // max: double.infinity,
        //     // height: 500,
        //     // decoration: BoxDecoration(
        //     //   color: Colors.white,
        //     //   borderRadius: BorderRadius.circular(30),
        //     // ),
        //     // color: Colors.white,
        //   ),
        // ),
        Form(
          key: _accountEmailFormKey,
          autovalidate: true,
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              TextFormField(
                onChanged: _setEmail,
                validator: (String value) => validateEmail(value),
                decoration: InputDecoration(
                  hintText: "Eg: you@gmail.com",
                  labelText: "Email Id",
                ),
              ),
              createNextButton(context, _next)
            ],
          ),
        ),
      ],
    );
  }

  void _next() {
    if (_accountEmailFormKey.currentState.validate()) {
      widget.incrementPage();
      _accountEmailFormKey.currentState.save();
    }
  }
}
