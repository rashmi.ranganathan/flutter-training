import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/constants/constant.dart';
import 'package:flutter_navigation_widget/screens/createAccount/Widgets/accountConfirmation.dart';
import 'package:flutter_navigation_widget/screens/createAccount/Widgets/accountDetails.dart';
import 'package:flutter_navigation_widget/screens/createAccount/Widgets/accountEmail.dart';
import 'package:flutter_navigation_widget/screens/createAccount/Widgets/accountPassword.dart';
import 'package:flutter_navigation_widget/screens/createAccount/Widgets/formProgress.dart';
import 'package:flutter_navigation_widget/screens/createAccount/Widgets/scheduleEvent.dart';

class FormPage extends StatefulWidget {
  @override
  FormPageState createState() => FormPageState();
}

class FormPageState extends State<FormPage> {
  int _progress = 0;

  _incrementPage() {
    setState(() {
      _progress++;
    });
  }

  Widget getFormPage() {
    Widget widget;
    switch (_progress) {
      case 0:
        widget = AccountEmail(incrementPage: _incrementPage);
        break;
      case 1:
        widget = AccountPassword(incrementPage: _incrementPage);
        break;
      case 2:
        widget = AccountDetails(incrementPage: _incrementPage);
        break;
      case 3:
        widget = ScheduleEvent(incrementPage: _incrementPage);
        break;
      case 4:
        widget = AccountConfirmation();
    }
    return widget;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Form demo')),
      body: Container(
        padding: EdgeInsets.only(top: 20.0),
        decoration: new BoxDecoration(color: Colors.blueAccent),
        child: Column(
          children: <Widget>[
            Opacity(
              child: FormProgress(progress: _progress),
              opacity: _progress < accountCreationSteps.length ? 1.0 : 0.0,
            ),
            getFormPage(),
          ],
        ),
      ),
    );
  }
}
