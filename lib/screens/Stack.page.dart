import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/widgets/backgroundImage.dart';
import 'package:flutter_navigation_widget/widgets/card.dart';

class MyStack extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Hero(
      key: ValueKey('stack'),
      tag: 'stack',
      child: Scaffold(
        appBar: AppBar(
          title: Text("STACK"),
        ),
        body: Stack(
          children: <Widget>[
            MyBackGroundImage(),
            Positioned(
              bottom: 48.0,
              left: 10.0,
              right: 10.0,
              child: MyCard(),
            ),
          ],
        ),
      ),
    );
  }
}
