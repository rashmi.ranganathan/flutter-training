import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/screens/Opacity.page.dart';

class DashBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'Dashboard',
      child: Scaffold(
        appBar: AppBar(
          title: Text("DashBoard"),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              key: ValueKey('DashBoard_button_back'),
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back),
            ),
            RaisedButton(
              key: ValueKey('DashBoard_button_forward'),
              child: Icon(Icons.arrow_forward),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            MyOpacity(text: "From Dashboard")));
              },
            ),
          ],
        ),
      ),
    );
  }
}
