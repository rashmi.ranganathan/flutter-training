import 'package:flutter/material.dart';
import 'package:flutter_navigation_widget/widgets/dragBox.dart';

class MyDragable extends StatefulWidget {
  @override
  _MoveState createState() => _MoveState();
}

class _MoveState extends State<MyDragable> {
  Color caughtColor = Colors.grey;
  String caughtText = "Drag Here!";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: ValueKey('dragabble'),
      appBar: AppBar(
        title: Text("Dragabble"),
      ),
      body: Stack(
        children: <Widget>[
          DragBox(ValueKey('lightGreen_gragBox'), Offset(0.0, 0.0), 'Box 1',
              Colors.lightGreen),
          DragBox(ValueKey('deepOrange_gragBox'), Offset(100.0, 0.0), 'Box 2',
              Colors.deepOrange),
          DragBox(ValueKey('lightBlueAccent_gragBox'), Offset(200.0, 0.0), 'Box 3',
              Colors.lightBlueAccent),
          Positioned(
            left: 100.0,
            bottom: 0.0,
            child: DragTarget(
              onAccept: (Color color) {
                caughtColor = color;
                caughtText = ':)';
              },
              builder: (
                BuildContext context,
                List<dynamic> accepted,
                List<dynamic> rejected,
              ) {
                return Container(
                  width: 200.0,
                  height: 200.0,
                  decoration: BoxDecoration(
                    color:
                        accepted.isEmpty ? caughtColor : Colors.grey.shade200,
                  ),
                  child: Center(
                    child: accepted.isEmpty
                        ? Text(caughtText)
                        : Text("Drag Here!"),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
