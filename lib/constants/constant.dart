const String randomText =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.";

const List<String> routes = [
  'form',
  'counter',
  'list',
  'dashboard',
  'stack',
  'future',
  'opacity',
  'animation-icon',
  'page-view',
  'drag'
];

const accountCreationSteps = [
  'Email',
  'Password',
  'Personal Infromation',
  'Schedule KYC'
];

const String beerListUrl = 'https://api.punkapi.com/v2/beers';
