import 'package:flutter_navigation_widget/common/api.dart';
import 'package:flutter_navigation_widget/constants/constant.dart';
import 'package:flutter_navigation_widget/models/beer.dart';

Future<List<Beer>> getBeers() async {
  dynamic result = await get(beerListUrl);
  final List<Beer> beerList =
      List<Beer>.from(result.map((data) => Beer.fromJSON(data)));
  return beerList;
}
