# Flutter widgets 

An example Flutter application with material widgets along with navigation.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## How to start
`flutter run`

## Widget Examples 

Animated Icon
```
Hero
Draggable
Future Builder
Opacity & Animated Opacity
Page View
Stack
```

along with some basic widgets
```
Text
Icon Button
Raised Button
Grid View
Center
Align
Positioned
etc.
```
